package main.java.pageObjects;

public interface HomePageElements {
    String acceptCookies = "onetrust-accept-btn-handler";
    String searchIconXPath = "//*[@id=\"dnvgl\"]/header/section/button[2]/span[2]";
    String searchIconCSS = "button.the-header__panel-toggle.the-header__panel-toggle--search";
}
