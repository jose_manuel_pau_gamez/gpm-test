package main.java.pageObjects;

public interface SearchPageElements {
    String searchText = "label.search-field__label";
    String searchBox = "search-input";
    String searchButton = "#app-search > section > label.search-field__btn";
    String searchResult = "//*[@id=\"app-search\"]/main/section[2]/article[1]/a/h2";
}
