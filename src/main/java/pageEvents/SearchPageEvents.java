package main.java.pageEvents;

import org.testng.Assert;

import main.java.pageObjects.SearchPageElements;
import main.java.utils.ElementFetch;
import test.java.BaseTest;

public class SearchPageEvents {

    public boolean verifySearchOpenOrNot(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Verifying the search field is visible and open");
        return elementFetch.getWebListElements("CSS", SearchPageElements.searchText).size()>0;
//        Assert.assertTrue(elementFetch.getWebListElements("CSS", SearchPageElements.searchText).size()>0,"Search filed did not open");
    }


    public void enterSearchWord(String word){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Entering the word "+ word + " in the search field");
        elementFetch.getWebElement("ID",SearchPageElements.searchBox).sendKeys(word);
    }

    public void clickonSearchButton(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Cliking on the Search Button to search");
        elementFetch.getWebElement("CSS",SearchPageElements.searchButton).click();
    }

    public boolean verifySearchResults(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Verifying at least one search result is displayed to the user");
        return elementFetch.getWebListElements("XPATH", SearchPageElements.searchResult).size()>0;
    }

    public void clickonSearchResult(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Cliking on the first search result");
        elementFetch.getWebElement("XPATH",SearchPageElements.searchResult).click();
    }


}
