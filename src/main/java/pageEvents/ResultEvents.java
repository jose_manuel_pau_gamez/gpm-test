package main.java.pageEvents;

import main.java.pageObjects.ResultPageElements;
import main.java.pageObjects.SearchPageElements;
import main.java.utils.ElementFetch;
import org.testng.Assert;
import test.java.BaseTest;

public class ResultEvents {

    public void verifyResult(String word){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Verifying the word "+ word + " is on the search result page");
        Assert.assertTrue(elementFetch.getWebElement("ID", ResultPageElements.pageSectionText).getText().contains(word),"Search result does not contain the word " + word);
    }
}
