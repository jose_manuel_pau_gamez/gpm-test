package main.java.pageEvents;

import main.java.pageObjects.HomePageElements;
import main.java.utils.ElementFetch;
import test.java.BaseTest;

public class HomePageEvents {

    public void clickOnAcceptCookiesButton(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Clicking on accept cookies");
        elementFetch.getWebElement("ID", HomePageElements.acceptCookies).click();
    }

    public void clickOnSearchIconButton(){
        ElementFetch elementFetch = new ElementFetch();
        BaseTest.logger.info("Clicking on search icon button");
        elementFetch.getWebElement("CSS", HomePageElements.searchIconCSS).click();
    }
}
