package test.java;

import main.java.pageEvents.ResultEvents;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import main.java.pageEvents.HomePageEvents;
import main.java.pageEvents.SearchPageEvents;

public class SearchWordTest extends BaseTest {


    @Test
    @Parameters(value={"wordFound"})
    public void inputWordandFound(String wordFound) throws InterruptedException {
        this.searchWordandFound(wordFound);
    }

    @Test
    @Parameters(value={"wordNotFound"})
    public void inputWordandNotFound(String wordNotFound) throws InterruptedException {
        this.searchWordandNotFound(wordNotFound);
    }

    public void searchWordandFound(String s) {
        HomePageEvents homepageevents = new HomePageEvents();
        homepageevents.clickOnAcceptCookiesButton();
        homepageevents.clickOnSearchIconButton();

        SearchPageEvents searchevents = new SearchPageEvents();
        try {
            // verify the search does appear on the screen and click on the result
            if (searchevents.verifySearchOpenOrNot()) {
                searchevents.enterSearchWord(s);
                searchevents.clickonSearchButton();

                // verify the results are shown in the screen
                if (searchevents.verifySearchResults()){
                    //click on the first result
                    searchevents.clickonSearchResult();
                    ResultEvents resultevents = new ResultEvents();
                    resultevents.verifyResult(s);                }
                else{
                    System.out.println("Results are not shown in the search page!!!");
                    Assert.assertTrue(searchevents.verifySearchResults());
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("The input search field does not appear on the screen");
        }
    }

    public void searchWordandNotFound(String s) {
        HomePageEvents homepageevents = new HomePageEvents();
        homepageevents.clickOnAcceptCookiesButton();
        homepageevents.clickOnSearchIconButton();

        SearchPageEvents searchevents = new SearchPageEvents();
        try {
            //verify the search does not appear on the screen
            if (searchevents.verifySearchOpenOrNot()) {
                searchevents.enterSearchWord(s);
                searchevents.clickonSearchButton();

                // verify the results are not shown in the screen
                Assert.assertFalse(searchevents.verifySearchResults());

            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("The input search field does not appear on the screen");
        }
    }







    }
