package test.java;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import main.java.utils.Constants;

public class BaseTest {

    public static WebDriver driver;
    public ExtentHtmlReporter htmlReporter;
    public ExtentReports extent;
    public static ExtentTest logger;


    @BeforeTest
    public void beforeTestMethod(){
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir")+File.separator+"reports"+ File.separator+"AutomationReporter.html");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setDocumentTitle("GreenPowerMonitor Automation Report");
        htmlReporter.config().setReportName("Automation Test Results:");
        htmlReporter.config().setTheme(Theme.DARK);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("Automation Tester","Jose Manuel Pau");

    }

    @BeforeMethod
    @Parameters(value={"browserName","url"})
    public void beforeMethodMethod(String browserName, String url, Method testMethod){
        logger = extent.createTest(testMethod.getName());
        setupDriver(browserName);
        driver.manage().window().maximize();
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(16, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void afterMethodMethod(ITestResult result){
        if (result.getStatus()==ITestResult.SUCCESS){
            String methodName = result.getMethod().getMethodName();
            String logfinalText = "";
            if (methodName.equalsIgnoreCase("inputWordandFound")) {
                logfinalText = "Test Case: " + methodName + " Passed. The word has been found in the search result!";
            }
            else if (methodName.equalsIgnoreCase("inputWordandNotFound")){
                logfinalText = "Test Case: " + methodName + " Passed. The word has not been founded in the website";
            }
            Markup m = MarkupHelper.createLabel(logfinalText, ExtentColor.GREEN);
            logger.log(Status.PASS,m);
        }
        else if (result.getStatus()==ITestResult.FAILURE){
            String methodName = result.getMethod().getMethodName();
            String logfinalText = "";
            if (methodName.equalsIgnoreCase("inputWordandFound")) {
                logfinalText = "Test Case: " + methodName + " Failed. The word has not been found in the search result!";
            }
            else if (methodName.equalsIgnoreCase("inputWordandNotFound")){
                logfinalText = "Test Case: " + methodName + " Failed. The word has been founded in the website";
            }
            Markup m = MarkupHelper.createLabel(logfinalText, ExtentColor.RED);
            logger.log(Status.FAIL,m);
        }
        driver.quit();
    }

    @AfterTest
    public void afterTestMethod(){
        extent.flush();
    }

    public void setupDriver(String browserName){
        if(browserName.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver");
            driver = new ChromeDriver();
        }
        else if(browserName.equalsIgnoreCase("firefox")){
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "geckodriver");
            driver = new FirefoxDriver();
        }
        else{
            System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + File.separator + "drivers" + File.separator + "chromedriver");
            driver = new ChromeDriver();
            }
        }
    }
