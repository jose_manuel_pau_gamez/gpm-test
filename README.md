
# Green Power Monitor Test

In this file I explain how to execute and all the dependencies needed to run this 
proyect.


This are the dependencies you need ot install on your computer to run the proyect:

```
Java 
Maven
```

The versions used for implement and run this proyect had been:
```
Java openjdk version "11.0.11" 2021-04-20
Apache Maven 3.6.0
```

The next commands use Maven to build the download the libraries needed, build the proyect and execute the tests.

To configure the parameters for the execution you need to edit the testng.xml file:

```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
<suite name="Search Test Suite">
    <listeners>
        <listener class-name="main.java.utils.SuiteListener"/>
    </listeners>
    <test name="Search Test User Case Automation">
        <parameter name="browserName" value="chrome" />
        <parameter name="url" value="https://www.dnv.com/" />
        <parameter name="wordFound" value="Marina" />
        <parameter name="wordNotFound" value="zzz" />
        <classes>
            <class name="test.java.SearchWordTest"/>
        </classes>
    </test>
</suite>
```
In _**browserName**_ parameter you can choose between "_chrome_" and "_firefox_".

In _**url**_ parameter you need to specify the url to begin the testing. In this case the url from DNV-GL website

In _**wordFound**_ parameter you need to specify the word you want to test for finding this word.

In _**wordNotFound**_ parameter you need to specify the word you want to test for not finding this word.

To execute the test execute the next command:

```
mvn verify
```

Or you can simply execute through the IDE if you go to the testng.xml file, Right click and select the option Run

Once the test has been executed you can see the reporting from the the tests executed in the file named AutomationReporter.html located in the _reports_ folder.

If some test fails the framework would make a screenshoot and locate png file in _screenshoots_ folder. 

The framework is configured to retry the test one time for each failing test. If you wish to configure the number or retries
you can modify the constant called _tries_  in the _Constants_ interface situated in _main.java/utils_ folder

**_Aclaration_**: To test the first test I only have entered to one search result to verify the search result has been done successfully but it's true  the test should verify all the search results to see if the word is on it. I haven't done this because is very inefficient doing that in one tab. It's true I would do that with multiple tabs on the navigator but this makes development a little difficult.

To reply the first three questions from the test I have written a document located in _doc_ folder called _TestPlan_plus_bug_report_

RELEASE NOTES
* 0.0.1
    * First version

* **Jose Manuel Pau** - *Initial work*